# Ember documentation

This is the documentation for [Ember](https://www.gmodstore.com/scripts/view/5620).

Here you will find instructions for installing, updating, configuring and extending Ember and its official modules and integrations.

::: tip
These instructions assume the reader knows the basics of working with their hosting environment, including a web server, PHP and MySQL.

Configuration of the aforementioned is covered only as it pertains to Ember, as it varies by environment otherwise.
:::

## Requirements

- a web server capable of URL rewriting and executing PHP scripts
- [PHP](https://www.php.net/manual/en/install.php) 7.4 through 8.1
- [MySQL](https://dev.mysql.com/doc/refman/8.4/en/installing.html) 5.7.8 or above

### PHP extensions

#### Required

- [cURL](https://www.php.net/manual/en/book.curl.php)
- [PDO MySQL](https://www.php.net/manual/en/ref.pdo-mysql.php)
- [Document Object Model](https://www.php.net/manual/en/book.dom.php) (enabled by default)
- [SimpleXML](https://www.php.net/manual/en/book.simplexml.php) (enabled by default)

#### Recommended

- [OpenSSL](https://www.php.net/manual/en/book.openssl.php) and [Multibyte String](https://www.php.net/manual/en/book.mbstring.php) (Web Push notifications)
- [GMP](https://www.php.net/manual/en/book.gmp.php) or [BCMath](https://www.php.net/manual/en/book.bc.php) (performance)
- [Memcached](https://www.php.net/manual/en/book.memcached.php) ([cache](./configuration/cache.md))
