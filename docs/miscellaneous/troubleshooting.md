# Troubleshooting
Here you can find some common issues and steps for troubleshooting them.

## 500 internal server error
An internal server error might occur for a number of reasons. To determine the cause more detailed logs are needed. Fetch them using the instructions below. You can use them to debug the issue yourself or attach them to a support ticket.
* check `storage/logs/app.log`
* set `development_mode` from `config.php` to `true` and reload the page
  * if the error message doesn't change, try checking your PHP logs directly
* check your web server logs (they're usually located somewhere in `/var/log`. On managed hosting there's typically a control panel option for viewing them)

## Other files on the web server are inaccessible

::: tip Background information
Ember uses the [front controller](https://en.wikipedia.org/wiki/Front_controller) pattern meaning all incoming requests (barring static assets) need to be routed to the `public/index.php` file.

The default `.htaccess` file and other [configuration examples](../installation/web.md#apache) implement this.
:::

To configure the Apache web server to simultaneously serve other files you may either

- configure [VirtualHosts](https://httpd.apache.org/docs/2.4/vhosts/examples.html) with separate **DocumentRoots** or
- amend the default [`.htaccess`](https://httpd.apache.org/docs/2.4/howto/htaccess.html) rules by creating additional **RewriteConds** to exclude any desired (sub)directories from **both** of the **RewriteRules**
  - for example, the directory `example` could be excluded by prepending `RewriteCond %{REQUEST_URI} !example/` to **both** of the existing **RewriteRules**.

## Server offline

If server cards on the landing page are displaying "Offline" instead of the "Join" button, ensure that
* the game server is on and configured to listen for queries
* you've specified the correct IP address (or FQDN) and query port for the [server](/configuration/server.md)
* there's no firewall blocking connections on the relevant port and protocol (UDP for most games)
  * outbound connections on the PHP server
  * inbound connections on the game server

## Payments not appearing
If users are not receiving credits or packages after a successful payment, check for any error messages under **Admin** > **Store** > **Logs** > **Failed payment logs**. If nothing shows up, depending on your payment processor check the following
* [PayPal IPN history](https://www.paypal.com/cgi-bin/webscr?cmd=_display-ipns-history)
* or [Stripe webhook](https://dashboard.stripe.com/webhooks) logs
