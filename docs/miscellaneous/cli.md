# Command line interface

The `cli` file is a PHP script which is used to run Ember's command line interface. It can be executed using [PHP from the command line](https://www.php.net/manual/en/features.commandline.usage.php) in the directory in which Ember is installed.

## Listing commands

```console
php cli
```

## Maintenance mode

### Entering

```console
php cli down
```

### Exiting

```console
php cli up
```

::: warning
Database migrations are not run automatically when maintenance mode is active.
:::

## Database migrations

### Running manually

```console
php cli db:migrate
```

## [Task scheduler](../configuration/task-scheduler.md)

### Invoking once

This command should be run once every minute using a job scheduler.

```console
php cli schedule:run 
```

### Invoking repeatedly

Invokes the scheduler at the start of every minute. Intended for development.

```console
php cli schedule:work 
```
