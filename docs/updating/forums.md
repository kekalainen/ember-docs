# Forums DLC

## Preparation

Ensure you have the latest version of the [web instance](web.md) installed.

Download the new module files from [GmodStore](https://www.gmodstore.com/market/view/032db31b-5dec-42aa-82d4-6510c8a7ff4d).

## Updating from the previous release

If you're exactly one version behind, navigate to the the `update` directory and upload the contained `forums` directory to the `modules` directory on your web server.

::: warning
The `update` directory must not be used if you're not running the very previous release.
:::

## Updating from an older release

### Removing the old files

To ensure a clean update remove any unmodified files from previous installations.

### Applying the update

Upload the `forums` directory to the `modules` directory on your web server.

Navigate to your web instance with a browser and proceed with the updater.
