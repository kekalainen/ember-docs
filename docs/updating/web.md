# Web

## Preparation

Download the new files from [GmodStore](https://www.gmodstore.com/scripts/view/5620).

Take a look at `config.php` to see if new settings have been introduced since the last update. If so, copy the new settings into your existing `config.php` file.

::: tip
For large instances it's recommended to enter maintenance mode prior to uploading the new files and to perform database migrations manually using the [CLI](/miscellaneous/cli) afterwards.
:::

::: warning
Game server integrations and modules should be updated (if updates are available) along with the web instance to ensure compatibility.
:::

## Updating from the previous release

If you're exactly one version behind, navigate to the the `update` directory and upload the contents of the contained `web` directory to your web server.

::: warning
The `update` directory must not be used if you're not running the very previous release.
:::

## Updating from an older release

### Removing the old files

To ensure a clean update remove any unmodified files from previous installations.

`config.php`, `modules` and `storage` should be retained.

::: tip
The `vendor` directory can be left as is unless backend dependencies have been updated.
::::

### Applying the update

Upload the new files to your web server.

Navigate to your web instance with a browser and proceed with the updater.

::: tip
Alternatively run the [database migrations using the CLI](../miscellaneous/cli.md#database-migrations).
:::
