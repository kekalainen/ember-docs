# Rust (C#)
After finishing the web update you can proceed with the C# plugin update.

## Downloading the new plugin
Download the updated plugin from [uMod](https://umod.org/plugins/ember). Take a look at the [configuration section](https://umod.org/plugins/ember#configuration) to see if new settings have been introduced since the last update. If so, copy the new settings into your existing `oxide/plugins/ember.json` file.

## Applying the update
Move the new file to your Rust server's `oxide/plugins` directory replacing the old one.
