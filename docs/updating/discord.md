# Discord integration DLC

## Disclaimer

The Discord integration for Ember is not endorsed or created by Discord.

## Preparation

Ensure you have the latest version of the [web instance](web.md) installed.

Download the new files from [GmodStore](https://www.gmodstore.com/market/view/17baccb7-5e58-4205-acb9-6cfcc4d4b5cb).

Take a look at the changelog to see if new configuration options have been introduced since the last update. If so, copy the new options into your existing `config.php` and `config.json` file(s).

::: tip
If you're using the [shared bot instance](../installation/discord.md#adding-the-shared-bot), only web files need to be updated.

A new sync token can be found in `shared_sync_token.txt`. It needs to be entered to the `sync_token` field in the module's `config.php` file. Running the `setsynctoken` command again is not necessary.
:::

## Updating from the previous release

If you're exactly one version behind, navigate to the the `update` directory and upload the contents of the contained `discord-integration` and `bot` directories to your web server and bot directory, respectively.

::: warning
The `update` directory must not be used if you're not running the very previous release.
:::

## Updating from an older release

### Removing the old files

To ensure a clean update remove any unmodified files from previous installations.

`discord-integration/config.php` and `bot/config.json` should be retained.

### Applying the update

* Upload the `discord-integration` directory to the `modules` directory on your web server.
  * Navigate to your web instance with a browser and proceed with the updater.
* Replace the files of your bot with the files from the `bot` directory.
  * Run `npm ci` to install updated dependencies.
