# Web

## Preparation

Download the addon files from [GmodStore](https://www.gmodstore.com/scripts/view/5620).

Extract the downloaded ZIP archive to a convenient location.

## Web server configuration

Ember requires a web server capable of performing basic URL rewriting and executing PHP scripts. Configuration examples are provided for Apache, Caddy and NGINX. Note that they'll need to be adjusted for your environment.

### Apache

Ensure that [mod_rewrite](https://httpd.apache.org/docs/current/mod/mod_rewrite.html) is installed and enabled. Set the [AllowOverride](https://httpd.apache.org/docs/2.4/mod/core.html#allowoverride) configuration directive to `All` to enable parsing the `.htaccess` file shipped with Ember.

::: warning
If you're using [PHP-FPM](https://www.php.net/manual/en/install.fpm.php) and certain Apache modules, the [virtual host](https://httpd.apache.org/docs/2.4/mod/core.html#virtualhost) or global configuration may need to be amended with
- `CGIPassAuth On` (see [CGIPassAuth directive](https://httpd.apache.org/docs/2.4/mod/core.html#cgipassauth)) or
- `SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1`.
:::

::: tip
The default configuration captures all requests. See [serving other files](../miscellaneous/troubleshooting.md#other-files-on-the-web-server-are-inaccessible) if needed.
:::

### Caddy

An example [Caddyfile](https://caddyserver.com/docs/caddyfile) can be found below. Requires [PHP-FPM](https://www.php.net/manual/en/install.fpm.php).

```
example.com {
    root * /var/www/html/public
    php_fastcgi * 127.0.0.1:9000 {
        index index.php
        env SERVER_PORT 443
    }
    rewrite / /index.php?{query}
    file_server
}
```

### NGINX

An example [Server Block](https://www.nginx.com/resources/wiki/start/topics/examples/server_blocks/) can be found below. Requires [PHP-FPM](https://www.php.net/manual/en/install.fpm.php).

```
server {
    listen 80;
    server_name example.com;
    root /var/www/html/public;

    index /index.php;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```

## Setting up a MySQL database

You will need to create a database for Ember as well as a user that has full access to said database. Consult your hosting provider's documentation regarding the creation of those. The database and user can be called anything you'd like. Using the `utf8mb4_general_ci` collation is recommended. Note the IP or domain of your database if it's not running on the same network as your web server.

## Uploading the files

Open up the extracted archive. Navigate to the `web` subfolder. Move the **contents** of the subfolder (not the folder itself) to the **root directory** of your web space. This is often called something along the lines of `html`, `public_html` or `www`. Check with your web host if you can't find the correct directory.

```
/var/www/html
├── app
├── ...
├── config.php
└── ...
```

::: warning
Subdirectory installations are not supported.
:::

## Configuration file

You should now have the required files uploaded, including the `config.php` file. Open the file for editing and insert your Steam API key (get one from [here](https://steamcommunity.com/dev/apikey)) and the MySQL database configuration you acquired previously. To do that, simply replace the values (right side) of the array elements with your data.

## Initial setup

Navigate to the HTTP(S) URL of your web server using a browser. An updater with a list of dependencies should load. Proceed with the database migrations prompted by the updater (or [run them using the CLI](../miscellaneous/cli.md#database-migrations)).

Upon redirection, sign in with the Steam account to which your copy of this product is licensed. You should then be redirected to **Admin** > **General**. Using the options there you can configure Ember to your liking.

::: tip
If you receive a `403 Forbidden` error response after signing in, ensure you signed in with the licensee account as it is the only [super administrator](../configuration/roles-permissions.md#role-management-permissions) by default.
:::
