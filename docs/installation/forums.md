# Forums DLC

## Preparation

Ensure you have the latest version of the [web instance](web.md) installed.

Download the Forums DLC module files from [GmodStore](https://www.gmodstore.com/market/view/032db31b-5dec-42aa-82d4-6510c8a7ff4d). Extract the downloaded ZIP archive to a convenient location.

## Uploading the files

Open up the extracted archive. Move the `forums` subdirectory to the `modules` directory within the directory in which you installed Ember.

```
modules
└── forums
```

## Configuration

You should see a **Forums** option added to the **Admin** dropdown in the navigation bar. Using the options there you can configure Forums for Ember to your liking.

There should also be a **Forums** permission category in the [role manager](../configuration/roles-permissions.md#managing-roles). It's recommended to grant the `Everyone` role permissions to post, edit and react to posts.
