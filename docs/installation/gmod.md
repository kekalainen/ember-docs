# Garry's Mod (Lua)

After finishing the web installation you can proceed with the Lua addon installation.

## Creating the server

Within Ember's admin panel, [set up a new Garry's Mod server](../configuration/server.md). Note the token of the server you just created.

## Uploading the files

Open up the extracted archive. Navigate to the `lua` subdirectory. Move the `ember` subdirectory to the `addons` directory of your Garry's Mod server.

## Configuration

Run the following command in the game console (either on the server or as a [super admin](https://wiki.facepunch.com/gmod/Player:IsSuperAdmin) client)

```console
ember connect <"URL"> <"token">
```

with the URL of your web instance and the token of the server you created earlier, individually enclosed within quotation marks.

A success message should appear in the console.

::: tip
The URL and the token are persisted in [data storage](https://wiki.facepunch.com/gmod/File_Based_Storage).
:::

::: warning
The angle brackets `<>` signify required parameters; do not include them in the command.
:::
