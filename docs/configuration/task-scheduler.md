# Task scheduler

::: tip
The task scheduler runs background tasks such as [RCON](server.md#remote-console)-based [store package actions](package.md#package-actions).
:::

To enable the task scheduler, configure the [invocation command](../miscellaneous/cli.md#invoking-once) using a job scheduler.
