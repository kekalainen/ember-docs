# Store payments: BitPay

- Navigate to **Admin** > **Store** and select the **Settings** tab.
  - Select a currency that is supported by BitPay.
  - Enter the **API Token** from your [BitPay dashboard](https://bitpay.com/dashboard/merchant/api-tokens).
- Click on the **Save** button.
