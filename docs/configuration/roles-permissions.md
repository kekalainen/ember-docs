# Roles & permissions

## Managing roles
Roles can be managed using the **role manager** found in **Admin** > **Roles**.

![](../media/screenshots/role_manager.jpg)
* A new role can be created from the plus icon on the top left
* Roles can be reordered by dragging and dropping them on the sidebar
  * the most privileged roles should be on the top

## Assigning roles to users
Roles can be assigned to users using the profile **administration card** visible to administrators on **profile pages**.

![](../media/screenshots/profile_administration.jpg)

All roles are listed on the card. Clicking on one will grant/revoke it to the user.

## Role sync
Roles can be synchronized into in-game groups and back using the integration addons/plugins. The **in-game equivalent** field on the role manager should be filled with the **exact** name of the corresponding role in-game.

The server group sliders on the role manager can be used to sync specific roles only on certain groups of servers.

::: warning LIMITATIONS
Some games (e.g. Garry's Mod) only support one role correspondent per player.

Roles are prioritized based on the [hierarchy](#role-management-permissions) when synchronizing with a single in-game role.

See the [implementation notes](../extending/game-integrations.md#roles) for more information.
:::

## Permission hierarchy
Role permissions follow a hierarchy.
* Every user has the permissions assigned to the `Everyone` role (despite it not being shown on profiles).
* If a user has a role with a permission explicitly allowed, any other roles they might have with said permission disabled won't have any effect.
* Banned users (with the `global` or `web` scope) only have the permissions granted to the `Banned` role despite any other roles & permissions (including those from the `Everyone` role) they might have normally.

### Role management permissions
Roles follow a hierarchy based on their order in the [role manager](#managing-roles).

The **manage roles** and **manage user roles** permissions allow for managing the roles beneath the user's current role. Users can grant and revoke permissions which they inherit from any of their roles.

::: tip SUPER ADMINISTRATOR
The **super administrator** permission permits managing any role regardless of hierarchy and other permissions. The account using which you purchased a license for Ember is hardcoded to be a super administrator.
:::

::: danger PRIVILEGE ESCALATION
The **manage roles** permission — particularly in conjunction with [role sync](#role-sync), the **manage server groups** or the **manage store packages** permissions — can be exploited for gaining access to higher-order roles. Grant these permissions sparingly and consider pre-existing in-game roles when assigning servers to groups.
:::

### Forums DLC permissions
[Forums DLC](https://www.gmodstore.com/scripts/view/5876) permissions function similarly to regular permissions. However, besides global permissions from the role manager it's possible to assign category- and board-specific permissions.

![](../media/screenshots/forum_permissions_manager.jpg)

Rather than a switch, board/category permissions have three possible states (**deny**/**inherit**/**allow**). Permission inheritance: **board permissions > category permissions > global permissions**.

::: tip
Threads in all categories/boards are viewable by default. To restrict access, **deny** the view threads permission for **both** the `Everyone` and the `Banned` role and **grant** it for roles that should have access.
:::