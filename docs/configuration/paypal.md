# Store payments: PayPal

## Ember configuration

- Navigate to **Admin** > **Store** and select the **Settings** tab.
  - Enter the **primary** email address of your PayPal account.
  - Select a currency that is set up on your PayPal account.
  - Disable sandbox mode.
- Click on the **Save** button.

## PayPal configuration
- IPN URL
  - While still in the settings page, copy the IPN URL.
  - Enter the URL you copied to the [PayPal IPN settings page](https://www.paypal.com/cgi-bin/customerprofileweb?cmd=_profile-ipn-notify).
    ![](../media/screenshots/paypal_ipn_settings.jpg)
