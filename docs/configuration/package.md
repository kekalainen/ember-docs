# Store packages

- Navigate to **Admin** > **Store packages**, select a server and click on the `+` icon in the top left of the store package manager.
- Input fields will appear. Populate the fields with the details of the package.
  - Cost for the package in credits (100 credits = 1 unit of the selected currency).
  - On the **Actions** section you can configure the actions that will be run when the package is applied, for instance assigning a role and executing custom Lua.

::: tip
To make packages redeemable on multiple servers, assign the same token for the servers in the integration plugin/addon configuration (while retaining unique tokens in **Admin** > **Servers**).
:::

### Example configuration
![](../media/screenshots/store_package_manager.jpg)

## Package actions

Store packages may contain **actions** to be executed on the web server or on the game server associated with the package.

### Phases

The *store package purchase* ("package instance") lifecycle is divided into three phases: *redeemable*, *active* and *expiring*. Each *package action* is associated with a single phase.

- **Redeemable** actions execute once after the purchase/grant of a package instance. Newly created actions are executed for all existing owners during the validity of their package instances.
- **Active** actions execute every time a package instance's owner connects to the associated game server during the validity of their package instance.
- **Expiring** actions execute once after the expiry of a package instance. Newly created actions are executed only for package instances expiring after their creation.

::: tip
#### Renewal

Renewing an *active* or *redeemable* package instance sets its phase as *expiring* and creates a new *redeemable* instance, summing the remaining and new validity period.
:::

### User presence

Some actions allow specifying a user presence requirement.

User-presenceless ("offline") actions are executed regardless of the target user's presence on the game server.

::: warning LIMITATIONS
#### Hibernation

Empty servers cannot [poll for actions](../extending/game-integrations.md#polling) and therefore do not execute them regardless of their user presence requirement. If [RCON](./server.md#remote-console) and the [task scheduler](task-scheduler.md) is configured, some offline actions may still be executed through console commands.
:::

## Action list

Below is a partial overview of the package actions available.

### [Role](./roles-permissions.md) (grant/revoke)

- The specified role will be
  - granted to the user on the web instance immediately upon purchase
  - revoked during a [scheduler invocation](./task-scheduler.md) or [poll execution](../extending/game-integrations.md#polling) after expiry.
- A group with the name of the role's **in-game equivalent** will be assigned to / removed from the associated player in-game.
  - For Garry's Mod a [CAMI](https://github.com/glua/CAMI) group will be applied to support multiple administration mods, including [ULib](https://github.com/TeamUlysses/ulib)/[ULX](https://github.com/TeamUlysses/ulx).
  - For Rust an [Oxide](https://umod.org/documentation/plugins/permissions) group will be applied.

::: tip
[Role sync](./roles-permissions.md#role-sync) does **not** need to be enabled for package roles to be granted or revoked.
:::

::: warning LIMITATIONS
#### Games which only support a single role per player
If a package role is revoked and the current in-game role cannot be determined (e.g. the player is not in-game and [role sync](./roles-permissions.md#role-sync) is send-only or disabled) an arbitrary in-game role may be revoked.

See the [implementation notes](../extending/game-integrations.md#roles) for more on how multiple roles are handled in single-role games.
:::

### (Remote) console command

Console commands are available for all [RCON-configurable](./server.md#remote-console) games.

::: tip
Games with a native integration do not require RCON configuration as the command actions are also [dispatched through the game API](../extending/game-integrations.md#command-action).
:::

#### Attributes

Any attribute of the `StorePackagePurchase` model instance may be included in the command within brackets `{}`. Subattributes can be accessed with the `.` operator. Examples:

- `{user.name}`
- `{user.steamid64}`
- `{user.minecraft_uuid}`
- `{package.name}`
- `{package.cost_credits}`

#### Example commands

##### [Garry's Mod](../installation/gmod.md)

```console
say {user.name} ({user.steamid64}) redeemed package {package.name}
```

##### Minecraft

```console
say {user.name} ({user.minecraft_uuid}) redeemed package {package.name}
```

##### [Rust](../installation/rust.md)

```console
inventory.giveto {user.steamid64} syringe.medical 5
```

### Code

Using the code action you can integrate Ember with other addons which aren't supported out of the box.

[Attributes](#attributes) are also supported for code actions.

#### Garry's Mod (GLua)

GLua is the programming language used for Garry's Mod addons.

[Introductory documentation can be found here.](https://wiki.facepunch.com/gmod/Beginner_Tutorial_Intro)

##### Example code

```lua
player.GetBySteamID64('{user.steamid64}'):Say("{package.name} was redeemed for me.")
```

### Permanent weapons

Weapons which are [given](https://wiki.facepunch.com/gmod/Player:Give) to the player on each [loadout event](https://wiki.facepunch.com/gmod/GM:PlayerLoadout) during the validity of the package.

(Cannot be [dropped](https://darkrp.miraheze.org/wiki/Hooks/Server/canDropWeapon) in DarkRP when `GM.Config.dropspawnedweapons` is set to `false`.)
