# Store payments: Stripe

## Ember configuration

- Navigate to **Admin** > **Store** and select the **Settings** tab.
  - Select a currency that is set up on your Stripe account.
  - Enter the **Publishable key** and **Secret key** from your [Stripe dashboard](https://dashboard.stripe.com/apikeys).
- Click on the **Save** button.

## Stripe configuration

- While still in the **Settings tab**, copy the **Webhook Endpoint URL**.
- Browse to the [Webhooks page](https://dashboard.stripe.com/webhooks) in Stripe's dashboard.
  - Hit **Add endpoint** and enter the URL you copied earlier.
  - Choose `checkout.session.completed` and `charge.dispute.funds_withdrawn` from the `Events to send` dropdown.
    ![](../media/screenshots/stripe_webhooks.jpg)
- Retrieve the Webhook **Signing secret** from [Stripe's dashboard](https://dashboard.stripe.com/webhooks) and enter it in Ember's configuration.
  ![](../media/screenshots/stripe_webhook_signing_secret.jpg)
