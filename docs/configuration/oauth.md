# OAuth
Here you can find OAuth configuration instructions to allow users to authenticate via third party providers.

::: tip
By default, users can sign in only through Steam and use other providers to link accounts. To enable using other providers for authentication, set the **Authentication provider** to `Any` in **Admin** > **General**.
:::

## Steam
* register for a [Steam Web API Key](https://steamcommunity.com/dev/apikey)
* assign the API key in Ember's `config.php` file

## Discord
* create an application on [Discord Developer Portal](https://discordapp.com/developers/applications)
* copy the client ID & client secret from the **OAuth2** tab to Ember's `config.php` file
* assign a redirect to `http(s)://example.com/auth/discord` from the **OAuth2** tab
  * replace `example.com` with the domain of your Ember installation
  ![](../media/screenshots/discord_oauth.png)

::: tip
It's possible to [add members to your Discord guild](https://discord.com/developers/docs/resources/guild#add-guild-member) after the OAuth flow. To enable the feature, fill out the `bot_token` and `guild_id` fields in Ember's `config.php` file. A corresponding [bot](https://discord.com/developers/docs/intro#bots-and-apps) needs to be a member of the guild with the `CREATE_INSTANT_INVITE` permission.
:::
