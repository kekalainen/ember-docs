# Cache

::: tip
Caching significantly reduces the amount of API requests and queries made to the database and game servers, thus increasing performance when generating pages.
:::

By default Ember stores cache files in the `storage/cache` directory.

Memory caching can be configured for increased performance. Drivers are available for [Memcached](https://memcached.org) and [Redis](https://redis.io).

Cache configuration is located in the `config.php` file.

::: warning
[Twig templates](/extending/overriding.html#twig) are cached using files regardless of the cache configuration.
:::

### Example configuration
```php
'cache' => [
    'cache.default' => 'file', // 'file', 'redis', 'memcached' or 'array' (no cache)
    'database.redis' => [
        'cluster' => false,
        'default' => [
            'host' => '127.0.0.1',
            'password' => null,
            'port' => 6379,
            'database' => 0
        ]
    ],
    'cache.stores.memcached' => [
        'servers' => [
            [
                'host'   => '127.0.0.1',
                'port'   => 11211,
                'weight' => 100
            ]
        ]
    ]
],
```
