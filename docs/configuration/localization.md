# Localization

The user-facing web pages of Ember can be translated from **Admin** > **Lozalication**.

![](../media/screenshots/localization_settings.jpg)

- Specify a [BCP 47](https://tools.ietf.org/html/rfc5646) locale string and click the save button.
  - The locale string affects timestamps and determines the language for the translations below.
- Enter translations for strings. The translations are saved automatically.
