# Webhooks

Webhooks are user-defined HTTP callbacks. They are triggered by database model (an object representing a table of data) lifecycle events (`created`, `updated`, `saved` and `deleted`).

Webhooks can be defined in **Admin** > **Webhooks**.

# Expressions
Expressions can be used in the condition, the URL and the request body.
## Attributes
Database model attribute names can be determined by inspecting the database schema (table column names) and model definitions.
* Attributes can be embedded in an expression using two pairs of curly brackets (`{{}}`).
* Attributes can change their value during an event. The previous value is accessible by appending `_original` to the attribute name.
* Subattributes can be accessed with the `.` operator.

Examples: `{{ id }}`, `{{ user.name }}`, `{{ thread.board.name }}` and `{{ thread.board_original.name }}`.
## Filters
Filters can be applied to an expression using the pipe operator (`|`). Some filters have arguments that can be passed within parentheses `()`. Examples: `{{ name | upper }}`, `{{ name | lower | capitalize }}` and `{{ name | truncate(5) }}`.

The following filters are available:
* `truncate`
* `lower`
* `upper`
* `capitalize`
* `divide`
* `default`
* `unix`.

# Condition
The condition is a series of boolean and relational expressions that are evaluated to determine whether to trigger the webhook.
In addition to the expressions above, the condition supports expressions with the following operators:
* relational operators equals `==` and NOT equals `!=`
* logical operators AND `&&` and `||` OR.

::: tip
The logical AND operator takes precedence. That is, AND expressions are evaluated before OR expressions.
:::

# Examples
## Send new forum posts to Discord
* **Model**: `ForumPost`
* **Event**: Created
* **Condition**: `{{ thread.bid }} == 1` (only include posts from the board with the ID `1`)
* **URL**: obtain from [Discord **Server Settings**](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)
* **Body**:
```json
{
    "embeds": [
        {
            "title": "{{ thread.topic }}",
            "url": "https://example.com/forums/posts/{{ pid }}",
            "description": "{{ content | truncate(150) }}",
            "timestamp": "{{ timestamp }}Z",
            "footer": {
                "text": "Posted by {{ user.name }}",
                "icon_url": "{{ user.avatar }}"
            }
        }
    ]
}
```

## Log Garry's Mod server map changes to Discord

* **Model**: `Server`
* **Event**: Updated
* **Condition**: `{{ metadata.map.name }} != {{ metadata_original.map.name  }}`
* **URL**: obtain from [Discord **Server Settings**](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)
* **Body**:
```json
{
    "embeds": [
        {
            "title": "{{ name }}",
            "description": "Now playing {{ metadata.game.mode }} in `{{ metadata.map.name }}`",
            "thumbnail": {
                "url": "https://example.com/api/loadingscreens/mapicons/{{ metadata.map.name }}"
            }
        }
    ]
}
```

## Log payments to Discord

* **Model**: `StorePayment`
* **Event**: Created
* **URL**: obtain from [Discord **Server Settings**](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)
* **Body**:

```json
{
    "embeds": [
        {
            "title": "Payment received",
            "url": "https://example.com/users/{{ user.id }}/purchases",
            "description": "{{ total | divide(100) }} {{ currency | upper }} using {{ processor | capitalize }}",
            "footer": {
                "text": "Paid by {{ user.name }}",
                "icon_url": "{{ user.avatar }}"
            }
        }
    ]
}
```
