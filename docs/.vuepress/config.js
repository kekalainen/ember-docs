import { defaultTheme } from '@vuepress/theme-default';
import { mediumZoomPlugin } from '@vuepress/plugin-medium-zoom';
import { searchPlugin } from '@vuepress/plugin-search';

export default {
    title: 'Ember documentation',
    description: 'Installation and configuration instructions for Ember',
    dest: 'public',
    plugins: [mediumZoomPlugin(), searchPlugin()],
    head: [['link', { rel: 'icon', href: '/ember.ico' }]],
    theme: defaultTheme({
        repo: 'https://gitlab.com/kekalainen/ember-issues',
        repoLabel: 'Issue tracker',
        docsRepo: 'https://gitlab.com/kekalainen/ember-docs',
        docsBranch: 'master',
        docsDir: 'docs',
        editLink: true,
        lastUpdated: true,
        danger: 'WARNING',
        navbar: [{ text: 'Discord', link: 'https://discord.gg/g5C4SzT' }],
        sidebar: {
            '/': [
                {
                    text: 'Installation',
                    collapsible: false,
                    sideBarDepth: 2,
                    children: [
                        '/installation/web',
                        '/installation/gmod',
                        '/installation/rust',
                        '/installation/forums',
                        '/installation/discord',
                    ],
                },
                {
                    text: 'Updating',
                    collapsible: false,
                    children: [
                        '/updating/web',
                        '/updating/gmod',
                        '/updating/rust',
                        '/updating/forums',
                        '/updating/discord',
                    ],
                },
                {
                    text: 'Configuration',
                    collapsible: false,
                    children: [
                        '/configuration/cache',
                        '/configuration/localization',
                        '/configuration/oauth',
                        '/configuration/roles-permissions',
                        '/configuration/server',
                        '/configuration/package',
                        '/configuration/bitpay',
                        '/configuration/paypal',
                        '/configuration/stripe',
                        '/configuration/task-scheduler',
                        '/configuration/webhooks',
                    ],
                },
                {
                    text: 'Extending',
                    collapsible: false,
                    children: [
                        '/extending/modules',
                        '/extending/game-integrations',
                        '/extending/overriding',
                    ],
                },
                {
                    text: 'Miscellaneous',
                    collapsible: false,
                    children: [
                        '/miscellaneous/cli',
                        '/miscellaneous/troubleshooting',
                    ],
                },
            ],
        },
    }),
};
